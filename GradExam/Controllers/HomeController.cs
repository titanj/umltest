﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GradExam.Models;
using GradExam.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using GradExam.ViewModels;

namespace GradExam.Controllers
{
    public class HomeController : Controller
    {
        private IGradeExamRepository context;

        public HomeController(IGradeExamRepository context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var vm = new HomeIndexViewModel
            {
                Users = context.ReadAllUsers().ToList()
            };

            return View(vm);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Making sure the Roles work
        [Authorize(Roles = "Admin")]
        public IActionResult Admin()
        {
            return Content("Admin works");
        }

        [Authorize(Roles = "Director")]
        public IActionResult Director()
        {
            return Content("Director works");
        }

        [Authorize(Roles = "Instructor")]
        public IActionResult Instructor()
        {
            return Content("Instructor works");
        }

        [Authorize(Roles = "Basic")]
        public IActionResult Basic()
        {
            return Content("It works");
        }
        #endregion

    }
}
