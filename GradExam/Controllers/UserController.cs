﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GradExam.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private IGradeExamRepository context;

        public UserController(IGradeExamRepository context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var model = context.ReadAllUsers();
            return View(model);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                context.CreateUser(user);
                return RedirectToAction("Index");
            }
            return View(user);
        }

        public IActionResult Edit(string id)
        {
            var model = context.Read(id);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(int id, User user)
        {
            if(!ModelState.IsValid)
            {
                return RedirectToAction("Edit", id);
            }

            context.Update(id, user);
            return RedirectToAction("Index");
        }

        
    }
}