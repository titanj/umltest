﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using GradExam.Services;
using Microsoft.AspNetCore.Mvc;

namespace GradExam.Controllers
{
    public class CourseController : Controller
    {
        private IGradeExamRepository context;

        public CourseController(IGradeExamRepository context)
        {
            this.context = context;
        }

        public IActionResult Delete(int id)
        {
            var course = context.ReadCourse(id);
            if (course == null)
            {
                return RedirectToAction("Index");
            }
            return View(course);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            context.DeleteCourse(id);
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            var course = context.ReadCourse(id);
            if (course == null)
            {
                return RedirectToAction("Index");
            }
            return View(course);
        }

        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Add(Course course)
        {

            if (ModelState.IsValid)
            {
                context.AddCourse(course);
                return RedirectToAction("Index");
            }
            return View(course);

        }
    }
}