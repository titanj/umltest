﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Models
{
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Semester { get; set; }
        public string Section { get; set; }
        public string InstructorName { get; set; }
        public Concentration Concentration { get; set; }
    }
}
