﻿using GradExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.ViewModels
{
    public class HomeIndexViewModel
    {
        public List<User> Users { get; set; }
    }
}
