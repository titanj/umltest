﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GradExam.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GradExam.Services
{
    public class DbGradExam : IGradeExamRepository
    {
        private ApplicationDbContext _db;

        public DbGradExam(ApplicationDbContext db)
        {
            _db = db;
        }

        public User CreateUser(User user)
        {
            _db.User.Add(user);
            _db.SaveChanges();
            return user;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteCourse(int id)
        {
            Course course = _db.Courses.Find(id);
            _db.Courses.Remove(course);
            _db.SaveChanges();
        }

        public void DeleteUser(int id)
        {
            var user = _db.User.Find(id);
            _db.User.Remove(user);
            _db.SaveChanges();
        }

        public User Read(string id)
        {
            return _db.User.FirstOrDefault(p => p.Id == id);
        }

        public ICollection<User> ReadAllUsers()
        {
            return _db.User.ToList();
        }

        public ICollection<Course> ReadAllCourses()
        {
            return _db.Courses.ToList();
        }

        public Course ReadCourse(int id)
        {
            return _db.Courses.FirstOrDefault(c => c.Id == id);
        }

        public void Update(int id, User user)
        {
            //var oldPerson = Read(id);
            //if (oldPerson != null)
            //{
            //    oldPerson.FirstName = user.FirstName;
                
            //    oldPerson.LastName = user.LastName;
            //    oldPerson.Email = user.Email;
            //    _db.SaveChanges();
            //}

        }

        public void UpdateCourse(int id, Course course)
        {
            var oldCourse = ReadCourse(id);
            if (oldCourse != null)
            {
                oldCourse.Name = course.Name;
                oldCourse.Section = course.Section;
                oldCourse.Semester = course.Semester;
                oldCourse.InstructorName = course.InstructorName;
                _db.SaveChanges();
            }

        }

        public Course AddCourse(Course Course)
        {
            _db.Courses.Add(Course);
            _db.SaveChanges();
            return Course;
        }
    }       
}           
