﻿using GradExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GradExam.Services
{
    public interface IGradeExamRepository
    {
        User CreateUser(User user);

        User Read(string id);
        Course ReadCourse(int id);
        ICollection<Course> ReadAllCourses();

        ICollection<User> ReadAllUsers();

        void Update(int id, User user);

        void Delete(int id);

        void DeleteCourse(int id);
        void UpdateCourse(int id, Course course);
        Course AddCourse(Course Course);

    }
}
